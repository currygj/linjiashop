import cartApi from '@/api/cart'
import axios from 'axios'

import { Checkbox, CheckboxGroup, Card, SubmitBar, Toast, NavBar, Tabbar, TabbarItem,Stepper, Button, Icon  } from 'vant';
const baseApi = process.env.VUE_APP_BASE_API
const baseUrl = 'http://192.168.18.209:8081'
import storage from '@/utils/storage'
export default {
    components: {
        [Card.name]: Card,
        [Checkbox.name]: Checkbox,
        [SubmitBar.name]: SubmitBar,
        [CheckboxGroup.name]: CheckboxGroup,
        [NavBar.name]: NavBar,
        [Tabbar.name]: Tabbar,
        [TabbarItem.name]: TabbarItem,
        [Stepper.name]: Stepper,
        [Button.name]: Button,
        [Icon.name]: Icon
    },

    data() {
        return {
            questionData: '',
            username: '',
            isLogin:false,
            infos: [],
            activeFooter: 2,
            checkedCartItem: [],//当前选中的购物车项目id
            allCartItem:[], // 用户所有的购物车项目id列表
            ConversationList: [],
            checkedAll: true,
            showEdit: false,
            rightText:'编辑'
        }
    },
    updated(){
        // 聊天定位到底部
        this.scrollToBottom()
        // this.$nextTick(() => {
        //     let msg = document.getElementById('chatContainer')
        //     console.log("now-scrollTop", msg.scrollTop)// 获取对象
        //     msg.scrollTop = msg.scrollHeight // 滚动高度
        //     console.log("new-scrollTop", msg.scrollTop)
        //     console.log("new-scrollHeight", msg.scrollHeight)
        // })
    },
    mounted(){
        this.init()
    },

    methods: {
        scrollToBottom(){
            window.scrollTo(0,document.documentElement.scrollHeight || document.body.scrollHeight);
        },
        addEvent: function() {
            this.submitQuestionData()
        },
        init(){
            // this.$nextTick(() => {
            //     let msg = document.getElementById('chatContainer') // 获取对象
            //     msg.scrollTop = msg.scrollHeight // 滚动高度
            //     console.log("scrollTop",msg.scrollTop)
            //     console.log("scrollHeight",msg.scrollHeight)
            // })

            console.log("执行 init() 函数===>")
            // 获取用户
            const user = storage.getUser()
            console.log("当前用户==> " + user)
            this.username = user.nickName
            this.isLogin = user.nickName
            if(this.isLogin) {

                // 从后端接口 获取 对话记录。所以 使用 Get 请求
                axios.get(baseUrl + '/chat/list', {
                    params: {
                        username: this.username
                        // username: 'Curry'
                    }
                })
                .then(response => {
                    // console.log("Curry的AI对话记录===>")
                    const data = response.data
                    this.infos = data
                    this.scrollToBottom()
                    /*for (let i = 0; i < data.length; i++) {
                        // 对话记录
                        const ConversationRecord = data[i]
                        // console.log(ConversationRecord)
                        for (const key in ConversationRecord) {
                            let val = ConversationRecord[key]
                             // 打印属性名和属性值
                            console.log(val);
                            // console.log(index + ": " + prop);
                        }
                    }*/
                    // console.log(this.infos[1]);
                    console.log("Curry的AI对话记录===> 遍历结束")
                })
                .catch((err) => {
                    console.error('请求失败信息',err);
                });
            }
        },
        // onClickLeft() {
            
        // },
                
        //  发布问题数据到后端接口，所以用post请求
        async submitQuestionData() {
            let url = baseUrl + '/chat/addChat'
            // let userMsg = this.questionData
            // console.log("用户问题=> " + userMsg)
            let params = {
                username: this.username,
                userMessage: this.questionData
            }
            // console.log("请求参数=> " + params.username + params.userMessage)
            for (const key in params) {
                let val = params[key]
                // 打印属性名和属性值
                console.log(key + ": " + val);
            }
            // 1.post请求，response 即为请求成功后后端返回的信息
            await axios.post(url, params).then(response => {
                console.log(response);
            }).catch(error => {
                console.log(error);
            })
            this.questionData = ''
            this.init()
        }
    }
};
