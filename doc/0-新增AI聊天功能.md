## 使用

### 1.增加Maven
```xml
        <!--        千帆大模型平台-->
        <dependency>
            <groupId>com.baidubce</groupId>
            <artifactId>qianfan</artifactId>
        </dependency>
```

### 2.使用指定大模型
```java
@Component
public class QianfanUtil {

    @Autowired
    Qianfan qianfan;

    public String addMessage(String content) {

        ChatResponse response = qianfan.chatCompletion()
//                .model("ERNIE-Speed-128k")  //使用model指定预置模型 默认模型是ERNIE-Bot-turbo
                .model("Qianfan-Chinese-Llama-2-7B")  //使用model指定预置模型 默认模型是ERNIE-Bot-turbo
                .addMessage("user", content) // 添加用户消息 (此方法可以调用多次，以实现多轮对话的消息传递)
                .temperature(0.7) // 自定义超参数
                .execute(); // 发起请求

        return response.getResult();
    }
}
```

### 3.在Controller中使用大模型
建表
```sql
create table `t_ai_conversation`(
    id int(4) PRIMARY KEY AUTO_INCREMENT ,
    username varchar(255) not null,
    user_message varchar(255) ,bot_message varchar(12000) ,
    create_time varchar(255)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

```

### 4.使用指定大模型

 addChat
 - 已经完成 查询和 新增 AI 对话
 - todo 接着完成前端后端的 数据交互工作

```shell
 /chat


Post
/addChat
 - Parameters
   - username
   - userMessage
 
 - Response Messages
   - List<Conversation> conversations
  

/list
 - Parameters
   - username
 
 - Response Messages
   - List<Conversation> conversations
   
```
### 5.测试
#### 5.1 测试API
```shell
测试GET
curl http://localhost:8081/chat/list?username=Curry

测试POST
curl -d "username=Curry&userMessage=请你从中医的角度，推荐缓解、治疗心血管疾病的茶品汤药"  http://localhost:8081/chat/addChat



https://cloud.tencent.com/developer/article/1870439
二、POST请求
POST请求的格式：curl -d "args" protocol://address:port/url

带参数的例子：

curl -d "user=admin&passwd=12345678" http://127.0.0.1:8080/login

POST数组,比如后端参数为 String[] itemNames,如果想传入a,b,c,d四个元素，这么写：

先发POST请求然后发GET请求
 - [!important]这是一种思路 先提交input 框中的提问信息 接着 GET 请求 list 接口
curl -d score=10 http://example.com/post.cgi --next http://example.com/results.html

```
### 5.测试
#### 5.1 测试API

### 12.解决Bug
```text
spring boot jpa 报错 :Executing an update/delete query

如果JPA提示Executing an update/delete query，那是一定是因为没有加@Transactional和@Modifying。

因为JPA没有事务支持，不能执行更新和删除操作。

所以反过来讲，就是在Service层或者Repository层上必须加@Transactional，来代表这是一个事务级别的操作，增删改查除了查都是事务级别的，就当这是一个规范也是ok的。
```


