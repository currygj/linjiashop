package cn.enilu.flash.ai.util;

import com.baidubce.qianfan.Qianfan;
import com.baidubce.qianfan.model.chat.ChatResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <h1> QianfanUtil </h1>
 * Created by GaoJia
 * Date  2024/4/14 23:14
 */
@Component
public class QianfanUtil {

    @Autowired
    Qianfan qianfan;

    public String addMessage(String content) {

        ChatResponse response = qianfan.chatCompletion()
//                .model("ERNIE-Speed-128k")  //使用model指定预置模型 默认模型是ERNIE-Bot-turbo
                .model("Qianfan-Chinese-Llama-2-7B")  //使用model指定预置模型 默认模型是ERNIE-Bot-turbo
//                .model("ERNIE-3.5-8K")  //使用model指定预置模型 默认模型是ERNIE-Bot-turbo
                .addMessage("user", content) // 添加用户消息 (此方法可以调用多次，以实现多轮对话的消息传递)
                .temperature(0.7) // 自定义超参数
                .execute(); // 发起请求

        return response.getResult();
    }
}
