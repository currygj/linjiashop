package cn.enilu.flash.ai.configuration;

import com.baidubce.qianfan.Qianfan;
import com.baidubce.qianfan.core.auth.Auth;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <h1> QianfanConfig </h1>
 * Created by GaoJia
 * Date  2024/4/14 23:31
 */
@Configuration
public class QianfanConfig {

    @Value("${QIANFAN_ACCESS_KEY}")
    String ak;

    @Value("${QIANFAN_SECRET_KEY}")
    String sk;

    @Bean
    public Qianfan qianFan() {
        return new Qianfan(Auth.TYPE_OAUTH, ak, sk);
    }
}
