package cn.enilu.flash.mobile.controller;

import cn.enilu.flash.ai.util.QianfanUtil;
import cn.enilu.flash.bean.entity.ai.Conversation;
import cn.enilu.flash.service.ChatService;
import cn.enilu.flash.utils.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <h1> ChatController </h1>
 * Created by GaoJia
 * Date  2024/4/14 22:58
 */
@RestController
@RequestMapping("/chat")
public class ChatController {
    @Autowired
    QianfanUtil qianfanUtil; //AI工具类

    @Autowired
    ChatService chatService;//数据库

    private String format(Date date){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }

    @GetMapping("/list")
//    public String getChat(HttpServletRequest request, Model model){
    public String getChat(@RequestParam("username") String username){
//    public String getChat(@RequestBody String username){

        List<Conversation> conversations=chatService.searchByUsername(username);
//        conversations.forEach(System.out::println);
        System.out.println("方便调试查看的json字符串：===》");
        String response = JsonUtil.toJson(conversations);
        System.out.println(response);
        return response;
    }

    @PostMapping("/addChat")
//    public String chat(@RequestParam("username") String username, @RequestParam("userMessage") String userMessage) {
    public String chat(@RequestBody Map<String, String> reqParams){

        String username= reqParams.get("username");
        String userMessage = reqParams.get("userMessage");
        System.out.println(username + "用户问题" +userMessage+ "\n\n");

        String botMessage = qianfanUtil.addMessage(userMessage);
        // todo 接着完成前端后端的 数据交互工作
//        System.out.println(botMessage+ "\n\n");
        int addChat = chatService.addChat(username, userMessage, botMessage, format(new Date()));
        if (1 == addChat) {
            return "ok";
        }
        return "failed";
    }
}
