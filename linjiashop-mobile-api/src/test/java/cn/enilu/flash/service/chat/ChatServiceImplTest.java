package cn.enilu.flash.service.chat;

import cn.enilu.flash.BaseApplicationStartTest;
import cn.enilu.flash.service.ChatService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class ChatServiceImplTest  extends BaseApplicationStartTest {

    @Autowired
    ChatService chatService;

    String username = "Curry";

    @Test
    public void searchByUsername() {
        chatService.searchByUsername(username).forEach(System.out::println);
    }

    @Test
    public void addChat() {
        String userMessage = "Curry-AI-你好-userMessage";
        String botMessage = "Curry-你好-botMessage";
        int addChat = chatService.addChat(username, userMessage, botMessage, format(new Date()));
        System.out.println(addChat);
    }

    private String format(Date date){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }
}