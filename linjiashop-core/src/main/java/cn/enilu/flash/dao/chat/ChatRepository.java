package cn.enilu.flash.dao.chat;

import cn.enilu.flash.bean.entity.ai.Conversation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <h1> ChatRepository </h1>
 * Created by GaoJia
 * Date  2024/4/15 00:49
 */
@Repository
public interface ChatRepository extends JpaRepository<Conversation, Integer> {

    // 查集合
    @Query(value = "SELECT * FROM t_ai_conversation WHERE username =?1", nativeQuery = true)
    List<Conversation> getByUsername(String username);

    @Transactional
    @Modifying
    @Query(value = "insert into `t_ai_conversation` (username, user_message," +
            " bot_message, create_time) values(?1,?2,?3,?4)",
            nativeQuery = true)
    int insert(String username, String userMessage, String botMessage, String createTime);

//    int insert(Conversation conversation);

}
