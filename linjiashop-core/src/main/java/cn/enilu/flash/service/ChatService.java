package cn.enilu.flash.service;

import cn.enilu.flash.bean.entity.ai.Conversation;

import java.util.List;

/**
 * <h1> ChatService </h1>
 * Created by GaoJia
 * Date  2024/4/15 00:09
 */
public interface ChatService {

    List<Conversation> searchByUsername(String username);

    int addChat(String username, String userMessage, String botMessage, String createTime);

//    int addChat(String username, String content, String res, String format);
}
