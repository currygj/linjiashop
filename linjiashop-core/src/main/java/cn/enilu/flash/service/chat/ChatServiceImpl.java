package cn.enilu.flash.service.chat;

import cn.enilu.flash.bean.entity.ai.Conversation;
import cn.enilu.flash.dao.chat.ChatRepository;
import cn.enilu.flash.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <h1> ChatServiceImpl </h1>
 * Created by GaoJia
 * Date  2024/4/15 00:10
 */
@Service
public class ChatServiceImpl implements ChatService {
    @Autowired
    ChatRepository chatRepository;

    @Override
    public List<Conversation> searchByUsername(String username) {
        return chatRepository.getByUsername(username);
    }

    @Override
    public int addChat(String username, String userMessage, String botMessage, String createTime) {
        return chatRepository.insert( username, userMessage, botMessage, createTime);
    }
}
