package cn.enilu.flash.bean.entity.ai;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.hibernate.annotations.Table;

import javax.persistence.*;
import java.io.Serializable;

/**
 * <h1> 聊天对话实体类 </h1>
 * Created by GaoJia
 * Date  2024/4/15 00:21
 */
@Entity(name = "t_ai_conversation")
@Table(appliesTo = "t_ai_conversation",comment = "聊天对话")
@Data
@AllArgsConstructor
public class Conversation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column
    private String username;

    @Column
    private String userMessage;

    @Column
    private String botMessage;

    @Column(name = "create_time",columnDefinition="DATETIME COMMENT '创建时间/注册时间'")
    private String createTime;

    public Conversation() {

    }
}
